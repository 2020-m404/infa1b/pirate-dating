﻿namespace Pirate_Dating
{
    class Ship
    {
        // Piraten, welche auf diesem Schiff segeln.
        public Pirate[] pirates;

        // The actual speed this ship is currently sailing.
        // Must be lower or equal than the ships max speed.
        public float actualSpeedInKnots;

        // The max speed this ship can sail.
        public float maxSpeedInKnots;

        // This is the default constructor (no parameters).
        // It gets inserted by the compiler, if the developer does not add a custom constructor.
        public Ship()
        {

        }
    }
}
