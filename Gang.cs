﻿namespace Pirate_Dating
{
    // Move class to separate file: CTRL + .
    class Gang
    {
        // Name of pirate gang
        public string name;

        // Main area, in which this gang is active
        public string mainArea;

        // Leader of pirate gang
        public Pirate leader;

        // Multiple pirates are member of a gang.
        // These members are stored in this array of pirates.
        public Pirate[] members;

        // Constructor of class Gang
        // Name of a constructor must be equal to the name of its class.
        // The constructor gets executed when a new object of this class is built.
        public Gang()
        {
            // Initialization of member field with empty array.
            // Instead of this statement, you could assign a default value to the members field.
            members = new Pirate[50];
            name = "Default name";
        }

        // Abbreviation for creating an empty constructor in VS: ctor (tab tab)
        // Constructor with one parameter of type string.
        // Use this constructor to initially assign a name to every gang.
        public Gang(string nameOfGang) : this()
        {
            // Before this constructor gets executed, the other constructor will be executed.
            // Use the THIS keyword to chain constructors.
            // The chained constructor gets selected based on its parameter list.
            name = nameOfGang;
        }
    }
}