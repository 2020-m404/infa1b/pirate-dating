﻿namespace Pirate_Dating
{
    class Weapon
    {
        // Type of weapon such as musket, pistol, sword, dagger
        public string type;

        // Strength of weapon on scale from 1 to 10 where 10 is strongest
        public float strength;

        public Weapon(float strength)
        {
            this.strength = strength;
        }
    }
}
