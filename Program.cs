﻿using System;
using System.Security.Cryptography;

namespace Pirate_Dating
{
    class Program
    {
        // Starting point of C# program
        // (the common language runtime CLR calls this "main method")
        static void Main(string[] args)
        {
            // Declare a variable of type pirate
            Pirate jack;

            // Assign a value to the declared value.
            // Use the new operator to create a new instance/object of a class.
            jack = new Pirate();

            // Access any member of an object using the member access operator (.)
            jack.name = "Jack";

            // Create an other variable (called emily) and initialize it with another pirate instance.
            Pirate emily = new Pirate();

            // Use the member access operator again to assign a string value ("Emily") as the new pirates value for its name field.
            emily.name = "Emily";

            // Assign the value of the second variable (emily) to the "inRelationWith" field of the first pirate (jack).
            jack.inLoveWith = emily;

            // TODO: Create an instance of class Gang and add at least one pirate to the gangs member field.
            Gang piratesOfTheCaribean = new Gang();
            piratesOfTheCaribean.members[0] = jack;
            piratesOfTheCaribean.members[4] = emily;

            Console.WriteLine($"Name of gang: {piratesOfTheCaribean.name}");

            // Create an instance of class Pirate with OBJECT INITIALIZER
            // One can use any public instance variables for object initializers.
            // If there's a parameter-less constructor, one may omit the parantheses.
            Pirate john = new Pirate() { name = "John", Age = 23 };
            john.Gang = piratesOfTheCaribean;
            piratesOfTheCaribean.leader = john;
            Console.WriteLine($"Is john a gang leader: {john.IsGangLeader}");


            // Property SailingOn is read-only (it has no setter).
            // john.SailingOn = new Ship(); // invalid code!

            // Constructors are executed first (and always).
            // Values of object initializers overwrite values set in constructors.
            Gang gangA = new Gang("Gang A") { name = "Gang a" };
            Console.WriteLine($"Name of gang: {gangA.name}");      // Name of gang: Gang a

            // A constructor must be called.
            Weapon sword = new Weapon(7.2f) { type = "Sword" };

            jack.Sleep(3.5f);
            jack.Sleep(10.0f);
            jack.Sleep(24);
        }
    }

}
