﻿using System;

namespace Pirate_Dating
{
    // Declare class named Pirate. As per convention, class names start with a capital letter.
    // Within a namespace, names of classes must be unique.
    // Convention: The opening bracket follows on the line after the class name.
    class Pirate
    {
        // Between the opening and closing brackets are MEMBERS of the class declared.

        // This is the declaration of a field which consists of:
        //  - access modifider (public)
        //  - type (string)
        //  - identifier (name)
        public string name;

        // Backing field for Age property.
        // Here, the actual age of the pirate is stored.
        private int age;

        // How old is the pirate: self-implemented property
        // Use the keyword value inside the setter to access the assigned value.
        // Validation (in setter): Pirates are not allowed to be younger than 0 yeard old :-)
        public int Age
        {
            get => age; // short for: { return age; }
            set
            {
                if (value >= 0)
                {
                    // value is valid --> assign to backing field
                    age = value;
                }

                // Age does NOT change if given value is invalid!
            }
        }

        // This is a PROPERTY with GETTER and SETTER accessor.
        // This kind of property is also called: read-write property.
        // For auto-implemented properties, the compiler generates a field
        // for storing and accessing the actual value.
        public Gang Gang { get; set; }

        // This is a read-only property. It hat no setter.
        // The only possibilities to set a value are as a default value
        // or in the constructor.
        public Ship SailingOn { get; } = new Ship();

        // What weapons does this pirate have
        public Weapon[] weapons;

        // With which (other) pirate is this pirate in relation (love)
        public Pirate inLoveWith;

        // Tiredness on scale from 0 to 10 (where 10 is very tired)
        private float tiredness = 5.0f;

        // This is a self-implemented property => The compiler won't generate a backing field.
        // Since it has only a getter, it is read-only.
        public bool IsGangLeader
        {
            get
            {
                // Determine whether THIS pirate is leader of a gang.
                return this == Gang.leader;
            }
        }

        public Pirate()
        {
            // In the constructor, one can assign values to read-only properties.
            SailingOn = new Ship();
        }

        public float Sleep(double durationInHours)
        {
            const float minTiredness = 0;
            const float recommendedHoursOfSleep = 8.0f;
            const float biologicalSleepFactor = 1.362f;  // pure fictional value :-)
            if (durationInHours > 0)
            {
                double recreation = (durationInHours / recommendedHoursOfSleep) * biologicalSleepFactor;
                tiredness = (float)Math.Max(minTiredness, tiredness - recreation);
            }
            return tiredness;
        }

    }
}
